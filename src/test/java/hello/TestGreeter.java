package hello;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// Change the comment again to kick off a build
// This change is made originally on the ted2branch
// comment!
// another comment but by Rocky this time!
public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }



   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }
   
   //@Test
   //@DisplayName("Test for Name='Rock'")
   //public void testGreeter2()
   //{
   //   g.setName("Rock");
   //   assertEquals(g.getName(),"Rock");
   //   assertEquals(g.sayHello(),"Howdy Rock!");
   //}
   // Buggy so I commented it out for the sake of it running

   // This is modified from Nat's test, just a heads up. Ran into problems with mine.
   @DisplayName("Test for Name != 'Number Two'")
   public void testGreeterEnterprise()
   {
      g.setName("William Riker");
      assertFalse(g.getName() == "Number Two");
      assertFalse(g.sayHello() == "Make it so, Number Two!");
   }

}
